# README #

This README documents the steps necessary to perform an outdoor site survey with the Cradlepoint R500-PLTE CBSD.

### What is this repository for? ###

* Quick summary

This script reads location data from a GPS puck and radio metrics from a Cradlepoint R500-PLTE device, both conected to a laptop (macOS tested). 

While this script was developed on macOS, with some changes, you can adapt to any Linux platform.

* Version

July 6, 2021

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

    * Bring up your Celona network.
    * Connect & verify a compatible GPS puck to laptop.
    * Connect via ethernet from laptop to the LAN port of the R500 device. Verify connectivity.
    
* Configuration

    * Modify the constants in this script as needed before running.
    * The GPS /dev handle, R500 default password, and R500 device for Celona are environment instance specific.
    
* Dependencies

    * PERL with Expect module installed
    
* How to run tests

    * ./cpr500-radio-survey.pl
    * redirect to file(s).  
        * For stopped tests, run for a few iterations with redirect to a new file, stop, then append iperf data.
        * After your test run, edit and run cpr500-parse-drive.sh and cpr500-parse-stop.sh against those files.
    * If everything is working as expected, you should see output similar to this in a loop:
    
```
...       
    "VER_PRETTY": "1.14.3.0",
    "VOICEREJECTMODE": "nochange",
    "WIMAX_REALM": "sprintpcs.com"
}
[admin@R500-PLTE-446: /]$
$GPGLL,3924.19407,N,11913.52904,W,045923.00,A,A*74



Data: -87,-7,23.4,100,315010,1,56040
```
    
    
    
* Deployment instructions

    * This script is intended to be run during continuous movement to record radio metrics at various locations relative to the access point.
    * The data from this script can be parsed into CSV files for overlay import into tools such as the Google Network Planner or Google Earth.
    * This script can also be run with fewer MAXITERATIONS for stopped movement tests in combination with iperf.

### Who do I talk to? ###

* Repo owner or admin

    * Mark Jimenez, mjimenez@celona.io
    
* Other community or team contact

    * Greg Yatman, greg@celona.io
    * The Frequency community at https://frequency.celona.io