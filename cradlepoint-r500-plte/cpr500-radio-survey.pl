#!/usr/bin/perl

## Edit these constants as needed before running the script
$R500_PASSWORD = "WA2106PA000426"; # on manufacturer sticker on bottom of device
$R500_DEVICE = "mdm-cd552586"; # ssh into device and find which device has "Celona"
$MAXITERATIONS = 10000; # usually never hit this on runs

use Expect;

my $exp = new Expect;
$command = "ssh";
$timeout = 10;
@parameters = ('admin@192.168.0.1');
$exp->raw_pty(1);  
$exp->spawn($command, @parameters)
  or die "Cannot spawn $command: $!\n";
sleep(3);
$exp->expect($timeout,"password")
 or die "Did not survive password\n";
$exp->send("$R500_PASSWORD\n");
$exp->expect($timeout,"\]")
 or die "No prompt 1\n";
sleep(2);

for ($iteration=0; $iteration < $MAXITERATIONS; $iteration++) {
  $gpsdata = `../gps/serial.py`;
  $exp->send("get status/wan/devices/$R500_DEVICE/diagnostics\n");
  $exp->expect($timeout,"]")
    or die "No prompt 2\n";
  $data = $exp->before();
  #print "get status data:\n".$data."\n";
  if($data =~ /RSRP\"\:\s+\"([\-\d]+)\"/) {
    $rsrp = $1;
  }
  if($data =~ /RSRQ\"\:\s+\"([\-\d]+)\"/) {
    $rsrq = $1;
  }
  if($data =~ /SINR\"\:\s+\"([\-\.\d]+)\"/) {
    $sinr = $1;
  }
  if($data =~ /SS\"\:\s+\"([\-\.\d]+)\"/) {
    $signalstr = $1;
  }
  if($data =~ /HM_PLMN\"\:\s+\"([\-\.\d]+)\"/) {
    $plmn = $1;
  }
  if($data =~ /PHY_CELL_ID\"\:\s+\"([\-\.\d]+)\"/) {
    $cellid = $1;
  }
  if($data =~ /RFCHANNEL\"\:\s+\"([\-\.\d]+)\"/) {
    $rfchannel = $1;
  }
  print "\n".$gpsdata."\n";
  print "Data: ";
  print "$rsrp,$rsrq,$sinr,$signalstr,$plmn,$cellid,$rfchannel\n";
  $rsrp = "NaN"; $rsrq = "NaN"; $sinr = "NaN"; $signalstr = "NaN"; $plmn = "NaN"; 
  $cellid = "NaN"; $rfchannel = "NaN";
  #sleep 5;
}
$exp->soft_close();
