# Welcome to the Celona Frequency community repository! #

### What is this repository for? ###

* Quick summary

This repository contains code as tools for working with a Celona network deployment, including automation for Celona and related 3rd party tools and devices.

* Version

July 6, 2021

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

At this time, subdirectories in this repository will contain instructions.  If you clone this repository, you will have everything we are sharing.  As we grow and as needed, we will break out into additional repositories. 

### Contribution guidelines ###

* Guidelines

At this time, this repository is read-only outside of the Celona organization.  When the time is right for contributions from the general community, we will let you know!  But if you have a pressing idea for contribution, please contact us (see below).

### Who do I talk to? ###

* Repo owner or admin
	* Mark Jimenez: mjimenez@celona.io
	* Greg Yatman: greg@celona.io
* Other community or team contact

Get more help from the Frequency community at https://frequency.celona.io/