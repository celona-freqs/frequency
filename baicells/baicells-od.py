#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from datetime import datetime
import os

os.system("../gps/serial.py /dev/cu.usbmodem14401")
driver = webdriver.Firefox()
driver.get("http://192.168.150.1")
time.sleep(3)
try:
    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "username"))
    )
    username = driver.find_element_by_id('username')
    username.clear()
    username.send_keys("admin")
    password = driver.find_element_by_id('password')
    password.clear()
    password.send_keys("admin")
    button = driver.find_element_by_xpath("//input[contains(@value,'Login')]")
 
    button.click()
    for iteration in range(30):
        time.sleep(3)
        
        rsrp1 = driver.find_element_by_xpath("//span[contains(@id,'rsrp1')]").text
        rsrp2 = driver.find_element_by_xpath("//span[contains(@id,'rsrp2')]").text
        rsrq = driver.find_element_by_xpath("//td[contains(@id,'rsrq')]").text
        sinr1 = driver.find_element_by_xpath("//td[contains(@id,'sinr1')]").text
        sinr2 = driver.find_element_by_xpath("//td[contains(@id,'sinr2')]").text
            
        plmn = driver.find_element_by_xpath("//td[contains(@id,'plmn')]").text
        earfcn = driver.find_element_by_xpath("//td[contains(@id,'earfcn')]").text
        cellid = driver.find_element_by_xpath("//td[contains(@id,'cellid')]").text
        pci = driver.find_element_by_xpath("//td[contains(@id,'pci')]").text
        dlfreq = driver.find_element_by_xpath("//td[contains(@id,'dlfreq')]").text
        ulfreq = driver.find_element_by_xpath("//td[contains(@id,'ulfreq')]").text
        bandwidth = driver.find_element_by_xpath("//td[contains(@id,'bandwidth')]").text
            
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(current_time + " " + rsrp1 + " " + rsrp2 + " " + rsrq + " " + sinr1 + " " + sinr2 + " " + plmn + " " + earfcn + " " + cellid + " " + pci + " " + dlfreq + " " + ulfreq + " " + bandwidth)
        driver.refresh()
    driver.close()
finally:
    driver.quit()
