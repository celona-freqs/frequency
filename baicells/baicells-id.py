#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from datetime import datetime
import os

os.system("../gps/serial.py /dev/cu.usbmodem14401")
driver = webdriver.Firefox()
driver.get("http://192.168.149.1")
time.sleep(5)
try:
    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "input_URN"))
    )
    username = driver.find_element_by_id('input_URN')
    username.clear()
    username.send_keys("admin")
    password = driver.find_element_by_id('input_PWD')
    password.clear()
    password.send_keys("admin")
    button = driver.find_element_by_class_name('login_btn')
    button.click()
    for iteration in range(10):
        time.sleep(3)
        element = WebDriverWait(driver, 10).until(
           EC.presence_of_element_located((By.ID, "intersinr"))
         )
                
        rsrp1 = driver.find_element_by_xpath("//span[contains(@id,'rsrp1')]").text
        rsrp2 = driver.find_element_by_xpath("//span[contains(@id,'rsrp2')]").text
        rsrp3 = driver.find_element_by_xpath("//span[contains(@id,'rsrp3')]").text
        rsrp4 = driver.find_element_by_xpath("//span[contains(@id,'rsrp4')]").text
        rssi = driver.find_element_by_xpath("//td[contains(@id,'interrssi')]").text
        rsrq = driver.find_element_by_xpath("//td[contains(@id,'interrsrq')]").text
        sinr = driver.find_element_by_xpath("//td[contains(@id,'intersinr')]").text

        plmn = driver.find_element_by_xpath("//td[contains(@id,'interPLMN')]").text
        earfcn = driver.find_element_by_xpath("//td[contains(@id,'interEARFCN')]").text
        cellid = driver.find_element_by_xpath("//td[contains(@id,'interCellid')]").text
        pci = driver.find_element_by_xpath("//td[contains(@id,'interpci')]").text
        dlfreq = driver.find_element_by_xpath("//td[contains(@id,'interFrequency')]").text
        ulfreq = driver.find_element_by_xpath("//td[contains(@id,'interULFreq')]").text
        bandwidth = driver.find_element_by_xpath("//td[contains(@id,'interBandwidth')]").text

        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(current_time + " " + rsrp1 + " " + rsrp2 + " " + rsrp3 + " " + rsrp4 + " " + rssi + " " + rsrq + " " + sinr + " " + plmn + " " + earfcn + " " + cellid + " " + pci + " " + dlfreq + " " + ulfreq + " " + bandwidth)
        driver.refresh()
    driver.close()
finally:
    driver.quit()
