# Welcome to the Celona Frequency community repository! #

### What is this repository for? ###

* Quick summary

This code polls Celona Edge, Access Point, and Device API endpoints every 5 seconds until Up (4) status, then will send an SMS to notify of the transition to the Up state.

A good use for this is when you are bringing up Edges, Access Points, and Devices.  The Up transition may take 3-15+ minutes.  Instead of human polling, you can setup this automated polling that will SMS you on transition to Up state.

* Version

March 17, 2022

### How do I get set up? ###

* Setup a Twilio account and verify you can send SMSs.
* If you do not have a Celona API key, create one and note it.
* modify the .env file with your phone number and Celona API key.
* Be able to access serial numbers for Edges, APs, and Devices in Celona via web UI or API.
* Note the serial number|IMSI of the component you want the script to watch.
* Run the python3 script via ./poll-[psenode|enodeb|device]-up.py <serial number|IMSI>
* Script will poll every 5 seconds for Up state, then will use your Twilio credentials to SMS you. 

### Contribution guidelines ###

At this time, this repository is read-only outside of the Celona organization.  When the time is right for contributions from the general community, we will let you know!  But if you have a pressing idea for contribution, please contact us (see below).

### Who do I talk to? ###

* Repo owner or admin
	* Mark Jimenez: mjimenez@celona.io
	* Greg Yatman: greg@celona.io
* Other community or team contact

Get more help from the Frequency community at https://frequency.celona.io/