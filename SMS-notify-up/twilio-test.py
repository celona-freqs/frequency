#!/usr/bin/python3

import sys
from twilio.rest import Client

MYBODY = sys.argv[1]
MYNUMBER = sys.argv[2]

account_sid = 'ACba<MY ACCOUNT SID>'
auth_token = 'b0<MY AUTH TOKEN>'
client = Client(account_sid, auth_token)

message = client.messages.create(
                              messaging_service_sid='MGd<MY MESSAGING SERVICE SID>',
                              body=MYBODY,
                              to=MYNUMBER
                          )

print(message.sid)