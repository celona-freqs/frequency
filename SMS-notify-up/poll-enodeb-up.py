#!/usr/bin/env python3
from decouple import config
import requests, sys
import json
import time
import os

SERNO = sys.argv[1]
ENDPOINT = 'enodebs'
API_KEY = config('KEY')
PHONE = config('PHONE')
API_DOMAIN = config('DOMAIN', default='api.celona.io')
my_headers = {'X-API-Key' : API_KEY}
url = "https://" + API_DOMAIN + "/v1/api/cfgm/" + ENDPOINT + "/" + SERNO
#print ("GET " + url + "\n")
status = -1

while status != 4:
    response = requests.get(url, headers=my_headers)
    json_formatted_str = json.dumps(response.json(), indent=2)
    #print(json_formatted_str)
    print(response.json()['data']['status'])
    status = response.json()['data']['status']
    if status != 4:
        time.sleep(10)
os.system("./twilio-test.py \"Celona Notification Service: Your Celona Access Point " +SERNO+ " is Up\" " +PHONE)