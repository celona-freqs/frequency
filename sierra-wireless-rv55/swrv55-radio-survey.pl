#!/usr/bin/perl

# In Services>AT (Telnet/SSH), set "Telnet" and "user"
$RV55_PASSWORD = "bH9Y8xHF25";
$RV55_IPADDRESS = "192.168.13.31";
$RV55_PORT = "2332";

$MAXITERATIONS = 10000; # usually never hit this on runs

use Expect;

sub getLoggingTime {

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
    #my $nice_timestamp = sprintf ( "%04d%02d%02d %02d:%02d:%02d",
    #                               $year+1900,$mon+1,$mday,$hour,$min,$sec);
    my $nice_timestamp = sprintf ( "%02d:%02d:%02d",
                                   $hour,$min,$sec);
    return $nice_timestamp;
}

my $exp = new Expect;
$command = "telnet";
$timeout = 60;
@parameters = ($RV55_IPADDRESS, $RV55_PORT);
$exp->raw_pty(1);  
$exp->log_stdout(0);
$exp->spawn($command, @parameters)
  or die "Cannot spawn $command: $!\n";
sleep(3);
$exp->expect($timeout,"Password:")
 or die "Did not survive password\n";
$exp->send("$RV55_PASSWORD\n");
$exp->expect($timeout,"OK")
 or die "No prompt 1\n";
sleep(2);

print "timestamp,rsrp,rsrq,sinr,rfchannel,cellid\n";

my $prev_rfchannel = -1;
my $prev_cellid = 1;

for ($iteration=0; $iteration < $MAXITERATIONS; $iteration++) {
  #$gpsdata = `../gps/serial.py`;

  #AT*CELLINFO2?
  #
  #ES: 1000Mb/s Full Duplex
  #NS: LTE-Advanced-Pro
  #LTECh: 55940
  #LTERm: false
  #LTECID: 258
  #LTELAC: 2301
  #LTEMCC: 315
  #LTEMNC: 10
  #LTEBSIC: 0
  #LTERSRQ: -7
  #LTERSRP: -89
  #LTESINR: 30.0
  #LTEBC: 168

  #OK

  
  $exp->send("AT*CELLINFO2?\n");

  $exp->expect($timeout,"OK")
    or die "No prompt 2\n";
  $data = $exp->before();

  #print $data;

  # so far connected case parse, need to check disconnected case
  if ($data =~ /LTECh:\s+([\d]+)/ ) {
    $rfchannel = $1;
  }
  if ($data =~ /LTECID:\s+([\d]+)/ ) {
    $cellid = $1;
  }
  if ($data =~ /LTERSRQ:\s+([\-\d]+)/ ) {
    $rsrq = $1;
  }
  if ($data =~ /LTERSRP:\s+([\-\d]+)/ ) {
    $rsrp = $1;
  }
  if ($data =~ /LTESINR:\s+([\-\d\.]+)/ ) {
    $sinr = $1;
  }

  my $timestamp = getLoggingTime();

  #print "\n".$gpsdata."\n";
  print "time:$timestamp,RSRP:$rsrp,RSRQ:$rsrq,SINR:$sinr,RFchannel:$rfchannel,cellid:$cellid ";
  if ($rfchannel != $prev_rfchannel) {
    print "new-rfchannel ";
  }
  if ($cellid != $prev_cellid) {
    print "new-cellid "
  }
  print "\n";
  $prev_rfchannel = $rfchannel;
  $prev_cellid = $cellid;
  $rsrp = "NaN"; $rsrq = "NaN"; $sinr = "NaN";  
  $cellid = "NaN"; $rfchannel = "NaN";
  #$pingData = `ping 192.168.30.14 -c 1`;
  #print "pingData: $pingData";
  sleep 1;
}
$exp->soft_close();
