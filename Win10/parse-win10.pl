#!/usr/bin/perl

# goal: parse and merge pertinent data from 2 files:
# 1) a data file containing gps+iperf data
# 2) a data file containing Win10 radio data
# Both file data are sycned via timestamp (to seconds resolution).
# However, RSRP data file has timestamps in 24hr format while
#   the iperf data file has timestamps in AM/PM format
# iperf3 in Win10 shows throughput differently than in linux
# in linux: result throughput on 2nd line
# in Win10: result throughput on 1st line
# This script will output GPS-latitude, GPS-longitude, 24hr timestamp, [radio metrics], iperf DL tp, iperf UL tp

# getopt options
# -i (REQUIRED) iperf (and GPS) data file path
# -r (REQUIRED) radio data file path
#   -v <debug level> verbosity, for debug.  Will not be good for CSV/KML output without grep.
#      1 for light debug
#      2+ for heavy line by line debug

use Getopt::Std;
use DateTime;

my $debug_mode = 0;
sub debug_print ($$) {
    my $debug_level_input = shift();
    my $text = shift();
    if ($debug_level_input <= $debug_mode) {
        print "DEBUG($debug_mode): ".$text;
    }  
}

my %options=();
getopts("i:r:v:", \%options);
if (defined($options{v})) {
    $debug_mode = $options{v};
    if (($debug_mode < 1) or ($debug_mode > 4)) {
        die "invalid debug mode ".$debug_mode;
    }
}
my $iperf_inputfile;
if (defined($options{i})) {
    $iperf_inputfile = $options{i};
} else {
    die "did not specify an iperf input file";
}
my $radio_inputfile;
if (defined($options{r})) {
    $radio_inputfile = $options{r};
} else {
    die "did not specify an radio input file";
}

# markjimenez@Marks-MacBook-Pro Win10 % file 2022-0106-gps-iperf-1.txt
# 2022-0106-gps-iperf-1.txt: Unicode text, UTF-16, little-endian text, with CRLF, CR line terminators, with overstriking
# ref: https://stackoverflow.com/questions/9712832/how-to-read-little-endian-utf-16-unicode-text-in-perl
open(IPERF_INPUTFILE, '<:encoding(UTF-16)',$iperf_inputfile) or die "Couldn't open file $iperf_inputfile, $!";
open(RADIO_INPUTFILE, "<$radio_inputfile") or die "Couldn't open file $radio_inputfile, $!";

# function: Read GPS+iperf file for this data:
    # * timestamp (AM/PM, covert to 24hr)
    # * GPGGA lat, long
    # * iperf DL tp, UL tp
# function: read radio file for
    # * timestamp (24hr)
    # * signal percent
    # * RSSI
    # * RCSP (RSRP equivalent)
# ??? Method 1: one instance of source1 relative to 2 sequential instances of source2
# Track last 2 timestamps worth of radio data and current_radio_timestamp (non-decreasing)
# Until either file is done reading,
    # Read from whichever file is tracking lowest timestamp
    # (determine timestamp_range of radio[1] and radio[2])?
    # GPS+iperf timestamp will be related to the 2 radio data as one of these:
        # Before both
            # if within timestamp_range, take radio[1], update current_radio_timestamp
            # else throw out GPS+iperf data 
        # Equal to radio[1], take radio[1], update current_radio_timestamp
        # Between radio[1] and radio[2]
            # take radio[x] which is closer, update current_radio_timestamp
        # Equal to radio[2], take radio[2], update current_radio_timestamp 
        # After radio[1] and radio[2]
            # if within timestamp_range, take radio[2], update current_radio_timestamp
            # else radio data needs to catch up
# ??? Method 2: user-defined timing tolerance, racing pointers to each of source1,2    
    # given time_tolerance
    # read first entry of radio and GPS+iperf data files
    # Until either file is done reading,
        # if timestamp.radio is within time_tolerance of timestamp.GPS+iperf
            # CSV out relevant data, set next read to "read both sources"
        # if "read both sources", then do that 
        # else Read from whichever file is tracking lowest timestamp

my $gps_iperf_timestamp = 0;
my $gps_lat = 0;
my $gps_long = 0;
my $iperf_dl_tp = 0;
my $iperf_ul_tp = 0;
my $iperf_dl_jitter = 0;
my $iperf_ul_jitter = 0;
# For iperf3 data
my $DOWNLOAD = "DL";
my $UPLOAD = "UL";
my $reverse_seen = "";
my $iperf_mode = "";

my $done_reading_files = 0;

sub read_iperf_header() {
#Connecting to host 192.168.1.46, port 5201
#Reverse mode, remote host 192.168.1.46 is sending
#[  4] local 12.1.1.31 port 50681 connected to 192.168.1.46 port 5201
#[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
#[  4]   0.00-3.00   sec  31.7 MBytes  88.7 Mbits/sec  0.183 ms  25362/50943 (50%)  
#- - - - - - - - - - - - - - - - - - - - - - - - -
#[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
#[  4]   0.00-3.00   sec  65.3 MBytes   183 Mbits/sec  0.093 ms  26270/52595 (50%)  
#[  4] Sent 52595 datagrams
#
#iperf Done.
#Connecting to host 192.168.1.46, port 5201
#[  4] local 12.1.1.31 port 50682 connected to 192.168.1.46 port 5201
#[ ID] Interval           Transfer     Bandwidth       Total Datagrams
#[  4]   0.00-3.00   sec  9.54 MBytes  26.7 Mbits/sec  7697  
#- - - - - - - - - - - - - - - - - - - - - - - - -
#[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
#[  4]   0.00-3.00   sec  9.54 MBytes  26.7 Mbits/sec  0.370 ms  0/7694 (0%)  
#[  4] Sent 7694 datagrams
    while(<IPERF_INPUTFILE>) {
        my $line = $_;
        debug_print(4,"read_iperf_inputfile:\n$line");
        if ($line =~ /Connecting\s+to\s+host/) {
            debug_print(2,"iperf3 CONNECTING line detected:\n$line");  
            $reverse_seen = 0; 
            $iperf_mode = ""; 
            $throughput = "0.0";
        }
        if ($line =~ /Reverse/) {
            debug_print(2,"iperf3 REVERSE line detected:\n$line");  
            $reverse_seen = 1;
        }
        if ($line =~ /local/) {
            debug_print(3,"iperf3 LOCAL line detected:\n$line");
            if ($reverse_seen == 1) {
                $iperf_mode = $DOWNLOAD;
                # perf data 2 lines away
                $line = <IPERF_INPUTFILE>;
                $line = <IPERF_INPUTFILE>;
                debug_print(2,"iperf3 download line:\n$line");
                #[  4]   0.00-3.00   sec  31.7 MBytes  88.7 Mbits/sec  0.183 ms  25362/50943 (50%)
                $iperf_dl_tp = 0;
                $iperf_units = "";
                $iperf_dl_jitter = 0;
                if ($line =~ /([\d\.]+)\s+(\w+)\/sec\s+([\d\.]+)\s+ms/ ) {
                    $iperf_dl_tp = sprintf("%3.1f",$1);
                    my $iperf_units = $2;
                    $iperf_dl_jitter = $3;
                    debug_print(2,"iperf3 throughput line detected: $line\n");
                    debug_print(3,"iperf3 tp: $iperf_dl_tp units: $iperf_units jitter:$iperf_dl_jitter\n");
                    # normalize to Mbits/sec
                    if ($iperf_units eq "Kbits") {
                        $iperf_dl_tp = sprintf("%3.1f",$iperf_dl_tp/1000.0);
                        debug_print(3,"iperf3 Kbits converted to Mbits, tp: $iperf_dl_tp\n");
                    }
                    if ($iperf_units eq "bits") {
                        $iperf_dl_tp = sprintf("%3.1f",$iperf_dl_tp/1000000.0);
                        debug_print(3,"iperf3 bits converted to Mbits, tp: $iperf_dl_tp\n");
                    }
                }
            } else {
                $iperf_mode = $UPLOAD;
                # perf data 5 lines away
                $line = <IPERF_INPUTFILE>;
                $line = <IPERF_INPUTFILE>;
                $line = <IPERF_INPUTFILE>;
                $line = <IPERF_INPUTFILE>;
                $line = <IPERF_INPUTFILE>;
                debug_print(2,"iperf3 upload line:\n$line");
                #[  4]   0.00-3.00   sec  9.54 MBytes  26.7 Mbits/sec  0.370 ms  0/7694 (0%)
                $iperf_ul_tp = 0;
                $iperf_units = "";
                $iperf_ul_jitter = 0;
                if ($line =~ /([\d\.]+)\s+(\w+)\/sec\s+([\d\.]+)\s+ms/ ) {
                    $iperf_ul_tp = sprintf("%3.1f",$1);
                    my $iperf_units = $2;
                    $iperf_ul_jitter = $3;
                    debug_print(2,"iperf3 throughput line detected: $line\n");
                    debug_print(3,"iperf3 tp: $iperf_ul_tp units: $iperf_units jitter:$iperf_ul_jitter\n");
                    # normalize to Mbits/sec
                    if ($iperf_units eq "Kbits") {
                        $iperf_ul_tp = sprintf("%3.1f",$iperf_ul_tp/1000.0);
                        debug_print(3,"iperf3 Kbits converted to Mbits, tp: $iperf_ul_tp\n");
                    }
                    if ($iperf_units eq "bits") {
                        $iperf_ul_tp = sprintf("%3.1f",$iperf_ul_tp/1000000.0);
                        debug_print(3,"iperf3 bits converted to Mbits, tp: $iperf_ul_tp\n");
                    }
                }
            }
            debug_print(2,"iperf3 mode detected iperf_mode: $iperf_mode\n");

            last;
        }
    }
}
sub month_text_to_numeric($) {
    my $month_text = shift;
    my %month_text_to_number = ( 'January' => 1,
                                 'February' => 2,
                                 'March' => 3,
                                 'April' => 4,
                                 'May' => 5,
                                 'June' => 6,
                                 'July' => 7,
                                 'August' => 8,
                                 'September' => 9,
                                 'October' => 10,
                                 'November' => 11,
                                 'December' => 12
    );
    if (exists $month_text_to_number{$month_text}) {
        return ($month_text_to_number{$month_text});
    } else {
        die "month_text_to_numeric: invalid month text: $month_text";
    }
}
sub read_gps_iperf_timestamp() {
    while(<IPERF_INPUTFILE>) {
        my $line = $_;
        debug_print(1,"read_gps_iperf_timestamp:\n$line");
        # January 6, 2022 2:35:16 PM
        if ($line =~ /(\w+)\s+(\d+)\,\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\w+)/) {
            my $gps_iperf_month_text = $1;
            my $gps_iperf_day = $2;
            my $gps_iperf_year = $3;
            my $gps_iperf_hour = $4;
            my $gps_iperf_minute = $5;
            my $gps_iperf_seconds = $6;
            my $gps_iperf_meridian = $7;
            if($gps_iperf_meridian eq "PM") {
                $gps_iperf_hour+=12;
            }
            my $gps_iperf_month = month_text_to_numeric($gps_iperf_month_text);
            debug_print(1,"month/day/year/hour/minute/seconds/meridian: $gps_iperf_month_text\($gps_iperf_month\) $gps_iperf_day $gps_iperf_year $gps_iperf_hour $gps_iperf_minute $gps_iperf_seconds $gps_iperf_meridian\n");    
            my $date_time = DateTime->new(
                year => $gps_iperf_year,
                month => $gps_iperf_month,
                day => $gps_iperf_day,
                hour => $gps_iperf_hour,
                minute => $gps_iperf_minute,
                second => $gps_iperf_seconds,
            );
            return($date_time->epoch);
        }
    } 
}
sub read_gps_iperf_inputfile() {
    $gps_iperf_timestamp = read_gps_iperf_timestamp();
    debug_print(1,"read_gps_iperf_inputfile gps_iperf_timestamp: $gps_iperf_timestamp\n");
    while(<IPERF_INPUTFILE>) {
        my $line = $_;
        debug_print(1,"read_gps_iperf_inputfile:\n$line");
        if (($line =~ /GPGLL\,([\d\.]+)\,(\w)\,([\d\.]+)\,(\w)/) or ($line =~ /GPGGA\,\d+\.\d+\,([\d\.]+)\,(\w)\,([\d\.]+)\,(\w)/)){
            debug_print(2,"GPS line detected:\n$line");
            $gps_lat = $1;
            $gps_lat_compass = $2;
            $gps_long = $3;
            $gps_long_compass = $4;
            debug_print(3,"GPS LAT:  $gps_lat $gps_lat_compass\n");
            debug_print(3,"GPS LONG: $gps_long $gps_long_compass\n");
        
            $lat_degrees = substr($gps_lat,0,2);
            $lat_minutes = substr($gps_lat,2,length($gps_lat-2));
            debug_print(3,"LAT_DEGREES $lat_degrees LAT_MINUTES $lat_minutes\n");      
            $lat_decimal_degrees = $lat_minutes / 60;
            if ($gps_lat_compass eq "S") {
                $decimal_lat = 0 - ($lat_degrees + $lat_decimal_degrees)    
            } else {
                $decimal_lat = $lat_degrees + $lat_decimal_degrees
            }
            $decimal_lat = sprintf("%2.5f",$decimal_lat);
            $gps_lat = $decimal_lat;
            debug_print(3,"Decimal LAT: $decimal_lat\n");

            $long_degrees = substr($gps_long,0,3);
            $long_minutes = substr($gps_long,3,length($gps_long-3));
            debug_print(3,"LONG_DEGREES $long_degrees LONG_MINUTES $long_minutes\n");      
            $long_decimal_degrees = $long_minutes / 60;
            if ($gps_long_compass eq "W") {
                $decimal_long = 0 - ($long_degrees + $long_decimal_degrees)    
            } else {
                $decimal_long = $long_degrees + $long_decimal_degrees
            }
            $decimal_long = sprintf("%3.5f",$decimal_long);
            $gps_long = $decimal_long;
            debug_print(3,"Decimal LONG: $decimal_long\n");
            last;
        }
    }
    # download always assumed before upload
    read_iperf_header(); 
    debug_print(3,"Finished reading DL\n");
    read_iperf_header(); 
    debug_print(3,"Finished reading UL\n");
    # should have all data for this context at this point
    debug_print(1,"read_gps_iperf_inputfile timestamp:$gps_iperf_timestamp lat:$gps_lat long:$gps_long iperf_dl_tp:$iperf_dl_tp iperf_dl_jitter:$iperf_dl_jitter iperf_ul_tp:$iperf_ul_tp iperf_ul_jitter:$iperf_ul_jitter\n");
}

my $radio_timestamp = 1;
my $radio_signal_percent = 0;
my $radio_rssi = 0;
my $radio_rscp = -999;
sub read_radio_timestamp() {
    while(<RADIO_INPUTFILE>) {
        my $line = $_;
        debug_print(1,"read_radio_timestamp:\n$line");
        # 2022-01-06 14:39:09
        if ($line =~ /(\d+)\-(\d+)\-(\d+)\s+(\d+)\:(\d+)\:(\d+)/) {
            my $radio_year = $1;
            my $radio_month = $2;
            my $radio_day = $3;
            my $radio_hour = $4;
            my $radio_minute = $5;
            my $radio_seconds = $6;
            debug_print(1,"year/month/day/hour/minute/seconds: $radio_year $radio_month $radio_day $radio_hour $radio_minute $radio_seconds\n");    
            my $date_time = DateTime->new(
                year => $radio_year,
                month => $radio_month,
                day => $radio_day,
                hour => $radio_hour,
                minute => $radio_minute,
                second => $radio_seconds,
            );
            return($date_time->epoch);
        }
    }  
}
sub read_radio_inputfile() {
    $radio_timestamp = read_radio_timestamp();
    $radio_signal_percent = 0;

    $radio_rssi = 0;
    $radio_rscp = -999;
    # Signal                 : 61%
    while(<RADIO_INPUTFILE>) {
        my $line = $_;
        debug_print(1,"read_radio_inputfile 1:\n$line"); 
        if ($line =~ /Signal\s+\:\s+(\d+)\%/) {
            $radio_signal_percent = $1;
            debug_print(1,"radio_signal_percent:$radio_signal_percent\n");
            last;
        }
    }

    # RSSI / RSCP            : 19 (-75 dBm)
    while(<RADIO_INPUTFILE>) {
        my $line = $_;
        debug_print(1,"read_radio_inputfile 2:\n$line"); 
        if ($line =~ /RSCP\s+\:\s+(\d+)\s+\(([\-\d]+)/) {
            $radio_rssi = $1;
            $radio_rscp = $2; 
            debug_print(1,"radio_rssi:$radio_rssi radio_rscp:$radio_rscp\n");
            last;   
        }
    }

    # should have all data for this context at this point
    debug_print(1,"read_radio_inputfile timestamp:$radio_timestamp radio_signal_percent:$radio_signal_percent radio_rssi:$radio_rssi radio_rscp:$radio_rscp\n");
}

print("timestamp,latitude,longitude,rscp,rssi,signal_percent,dl_tp,dl_jitter,ul_tp,ul_jitter\n");
while ($done_reading_files != 1) {
    read_gps_iperf_inputfile(); # read next entry
    if($gps_iperf_timestamp == 0) {
        last;
    }
    while ($radio_timestamp < $gps_iperf_timestamp) {
        read_radio_inputfile(); # read next entry
        if($radio_timestamp == 0) {           
            last;
        }
    }
    if (abs($gps_iperf_timestamp - $radio_timestamp) > 4) { # timestamp diff tolerance
        next;
    }
    if( ($gps_iperf_timestamp == 0) || ($radio_timestamp == 0)) {
        last;
    } 
    # should have all data for this context at this point
    debug_print(1,"main loop gps_iperf timestamp:$gps_iperf_timestamp lat:$gps_lat long:$gps_long iperf_dl_tp:$iperf_dl_tp iperf_dl_jitter:$iperf_dl_jitter iperf_ul_tp:$iperf_ul_tp iperf_ul_jitter:$iperf_ul_jitter\n");
    debug_print(1,"main loop radio timestamp:$radio_timestamp radio_signal_percent:$radio_signal_percent radio_rssi:$radio_rssi radio_rscp:$radio_rscp\n"); 
    print("$gps_iperf_timestamp,$gps_lat,$gps_long,$radio_rscp,$radio_rssi,$radio_signal_percent,$iperf_dl_tp,$iperf_dl_jitter,$iperf_ul_tp,$iperf_ul_jitter\n");
}

close(IPERF_INPUTFILE);
close(RADIO_INPUTFILE);