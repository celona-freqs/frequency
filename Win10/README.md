# Microsoft Windows 10 GPS, radio data, and iperf performance automation #

### What is this repository for? ###

This repository contains automation for gathering GPS coordinates, iperf3 throughput performance, and radio metrics (via netsh mbn show interfaces).  Since gathering GPS and radio data was done in two different environments (Powershell and MS-DOS shel), post-processing automation is also included to correlate GPS coordinates, iperf3 throughput data, and radio data into a comma-separated value (CSV) format.

With this capability, you can perform an outdoor walk/drive site survey with your CBRS-enabled Windows 10 laptop.  The scripts can also be adapted for indoor testing.

* Version

Jan 12, 2022

### How do I get set up? ###

The Powershell and batch files are intended to be run on a Windows 10 laptop with CBRS capability.

The PERL script is intended to be run on a linux/MacOS post-processing machine, with the data gathered in Windows 10 to be copied over to linux/MacOS. 

Copy all files except for the PERL script into a common directory.
Copy the PERL script into a supported linux/MacOS machine.

Verify that you have a working Celona/CBRS network to test your Windows 10 laptop.

Stand up an iperf3 server next to the CBRS access point (i.e. before the WAN uplink).

Plug in a USB GPS puck.  If needed, convert USB-A to USB-C or other type to plug into your Windows 10 laptop.  We use a budget GPS puck from Amazon ( https://www.amazon.com/dp/B073P3Y48Q?psc=1&ref=ppx_yo2_dt_b_product_details ).

In Windows 10, note the COM port for the GPS puck from `Settings > Devices > Bluetooth & other devices > Other devices`.  The GPS puck noted above should be listed as "USB Serial Device (COM<port number>)".

Edit `gps-iperf.ps1`. 
Modify the COM port for the $port variable to match that of the GPS puck.
Modify the filename for data redirection.

Open a Command Prompt shell.  Navigate to the common file directory and run `netsh-mbn-show-interfaces.bat >> <radio data filename of your choice>`.

Open a Windows PowerShell instance.  Navigate to the common directory and run `gps-iperf.ps1`.  This script automatically redirects data to the filename you edited, above.

Slowly perform your walk test.

When complete, stop both scripts via CTRL-C.  Copy both datafiles over to your post-processing linux/MacOS machine into the directory with the PERL script.  Run the PERL script with the following parameters:

-i <path to iperf+GPS datafile>

-r <path to radio datafile>

The PERL script should output CSV data that can be redirected into a file and push into supported visualization tools such as Google Network Planner or Google Earth.  The PERL script corelates GPS, iperf, and radio data via timestamps.  The script assumes a 4 second timestamp difference tolerance for valid entries.

Feel free to experiment by adjusting delay & timings between GPS data gathering and iperf, as well as iperf parameters.  On a test laptop, we we observed 3-4 seconds for the netsh command perl loop for gathering radio data (RSCP and RSSI).

### Contribution guidelines ###

* Guidelines

At this time, this repository is read-only outside of the Celona organization.  When the time is right for contributions from the general community, we will let you know!  But if you have a pressing idea for contribution, please contact us (see below).

### Who do I talk to? ###

* Repo owner or admin
	* Mark Jimenez: mjimenez@celona.io
	* Greg Yatman: greg@celona.io
* Other community or team contact

Get more help from the Frequency community at https://frequency.celona.io/