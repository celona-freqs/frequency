while (1 -ne 2) {
  date >> 2022-0106-gps-iperf-1.txt
  $port= new-Object System.IO.Ports.SerialPort COM6,9600,None,8,one
  $port.open()
  1..10 | % { $port.ReadLine() >> 2022-0106-gps-iperf-1.txt }
  $port.close()
  timeout /t 1
  .\iperf3 -c 192.168.1.46 -p 5201 -u -b 180m -i 3 -t 3 -R -l 1300 >> 2022-0106-gps-iperf-1.txt
  timeout /t 3
  .\iperf3 -c 192.168.1.46 -p 5201 -u -b 50m -i 3 -t 3 -l 1300 >> 2022-0106-gps-iperf-1.txt
  timeout /t 3
}