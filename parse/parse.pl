#!/usr/bin/perl

use Math::BigFloat;


# goal: site survey master parse program
# used to parse Celona Site survey automation data

# getopt options
#   WiP -O k: KML, invalid if no GPS data on output event
#      toc: (default) (csv)
#   -i <inputfile>
#   WiP -g once: process GPS line ONCE, even if there are more in the file
#        what this really means: the file represents ONE physical location point > calculate RMS for multiple data
#        good for stop survey if automation run multiple times and happens to dump GPS data multiple times
#        for a file resulting in multiple lines of output, they are assumed to be at the FIRST GPS location seen in the file.  
#        subsequent GPS locations after the first are ignored
#      each: (default) Each GPS line should be processed as separate output lines of data
#   -v <debug level> verbosity, for debug.  Will not be good for CSV/KML output without grep.
#      1 for light debug
#      2 for heavy line by line debug
#   -D: drive testing - one entry per radio data line vs RMS/mean on multiple lines for stopped testing

# types of data to handle:
# iperf, download and upload.  May have both, either, or none
# GPS data optional
#  First time GPS data seen, set flag to expect it or error out on output event
#  If GPS data seen, then if a new radio line appears without GPS data, then use the previous GPS line (multiple data at same point)
# Radio line, supported devices: Baicells ID/OD , termux telephony
#   track best RSRP RMS
#   track ALL reported RSRP values

use Getopt::Std;

my $debug_mode = 0;
sub debug_print ($$) {
    my $debug_level_input = shift();
    my $text = shift();
    if ($debug_level_input <= $debug_mode) {
        print "DEBUG($debug_mode): ".$text;
    }  
}

my $outputfile_type="";
my $inputfile="";
my %options=();
my $OUTPUTFILE_TYPE_KML = "kml";
my $OUTPUTFILE_TYPE_CSV = "csv";

my $notes;

my $gpsmode="";
my $GPSMODE_ONCE = "once";
my $GPSMODE_EACH = "each";

my $drive_mode_flag = 0;  # No RMS, a point per line parsed

getopts("O:i:g:v:n:D", \%options);
#print "-O $options{O}\n" if defined $options{O};
#print "-i $options{i}\n" if defined $options{i};
#print "-g $options{g}\n" if defined $options{g};
#print "-v $options{v}\n" if defined $options{v};
#print "-n $options{n}\n" if defined $options{v};
if (defined($options{O})) {
    $outputfile_type = $options{O};
} else {
    $outputfile_type = $OUTPUTFILE_TYPE_CSV; 
}
if (($outputfile_type ne $OUTPUTFILE_TYPE_CSV) and ($outputfile_type ne $OUTPUTFILE_TYPE_KML)) {
    die "invalid outputfile type ".$outputfile_type;
}
if (defined($options{i})) {
    $inputfile = $options{i};
} else {
    die "did not specify an input file";
}
if (defined($options{g})) {
    $gpsmode = $options{g};
} else {
    $gpsmode = $GPSMODE_EACH; 
}
if (($gpsmode ne $GPSMODE_EACH) and ($gpsmode ne $GPSMODE_ONCE)) {
    die "invalid gps mode ".$gpsmode;
}
if (defined($options{v})) {
    $debug_mode = $options{v};
    if (($debug_mode < 1) or ($debug_mode > 4)) {
        die "invalid debug mode ".$debug_mode;
    }
}
if (defined($options{n})) {
    $notes = $options{n};
}
if (defined($options{D})) {
    $drive_mode_flag = 1;
}

open(INPUTFILE, "<$inputfile") or die "Couldn't open file $inputfile, $!";

# zero out possible output data variables per "entry"
# when a line of data has been output, some data may have to be re-zeroed

# For GPGLL GPS data from GPS puck
my $decimal_lat = 0;
my $decimal_long = 0;

# cradlepoint R500 specific
my $signalstr = "";
my $rfchannel = "";
my $rsrp = "";
my $sinr = "";
my $signalstr_sum = 0;
my $num_signalstr_entries = 0;

# For Baicells OD04 data
my $time      = ""; 
my $rsrp1     = ""; 
my $rsrp2     = ""; 
my $rsrq      = ""; 
my $sinr1     = ""; 
my $sinr2     = ""; 
my $plmn      = ""; 
my $earfcn    = ""; 
my $cellid    = ""; 
my $pci       = ""; 
my $dlfreq    = ""; 
my $ulfreq    = "";      
my $bandwidth = ""; 
# power/signal rsrp/rsrq/sinr processing
my $rsrp_square_sum_all = 0; # for RSRP1 and RSRP2
my $rsrp_square_sum_best = 0; # for best of RSRP1 and RSRP2
my $num_rsrp_entries = 0;
my $rsrq_square_sum = 0;
my $num_rsrq_entries = 0;

my $sinr_sum_all = 0; # for RSRP1 and RSRP2
my $sinr_sum_best = 0; # for best of RSRP1 and RSRP2
my $num_sinr_entries = 0;

# For iperf3 data
my $DOWNLOAD = "DL";
my $UPLOAD = "UL";
my $reverse_seen = "";
my $iperf_mode = "";
my $throughput = "0.0";
my $iperf_throughput_flag = 0;
# iperf throghput processing
my $dl_square_sum = 0;
my $ul_square_sum = 0;
my $dl_entries_count = 0;
my $ul_entries_count = 0;
# termux processing
my $termux_rsrp = -999;
my $termux_latitude = 0;
my $termux_longitude = 0;
my $termux_registered = 0;
my $termux_entries_count = 0;

my $baicells_id_file_flag = 0;
my $baicells_od_file_flag = 0;
my $r500_file_flag = 0;
my $termux_file_flag = 0;

while(<INPUTFILE>) {
   $line = $_;
   debug_print(4,$line);
   my $baicells_id_seen_flag = 0;
   my $baicells_od_seen_flag = 0;
   my $iperf_throughput_flag = 0;
   my $termux_dbm_seen_flag = 0;
   my $r500_seen_flag = 0;
   

   # $GPGGA,140054.000,3948.4738,N,08609.4476,W,1,09,1.0,236.7,M,-33.7,M,,0000*60
   # parse GPGLL from GPS puck
   if (($line =~ /GPGLL\,([\d\.]+)\,(\w)\,([\d\.]+)\,(\w)/) or ($line =~ /GPGGA\,\d+\.\d+\,([\d\.]+)\,(\w)\,([\d\.]+)\,(\w)/)){
        debug_print(2,"GPGLL line detected:\n$line");
        $gpgll_lat = $1;
        $gpgll_lat_compass = $2;
        $gpgll_long = $3;
        $gpgll_long_compass = $4;
        debug_print(3,"GPGLL LAT:  $gpgll_lat $gpgll_lat_compass\n");
        debug_print(3,"GPGLL LONG: $gpgll_long $gpgll_long_compass\n");
       
        $lat_degrees = substr($gpgll_lat,0,2);
        $lat_minutes = substr($gpgll_lat,2,length($gpgll_lat-2));
        debug_print(3,"LAT_DEGREES $lat_degrees LAT_MINUTES $lat_minutes\n");      
        $lat_decimal_degrees = $lat_minutes / 60;
        if ($gpgll_lat_compass eq "S") {
            $decimal_lat = 0 - ($lat_degrees + $lat_decimal_degrees)    
        } else {
            $decimal_lat = $lat_degrees + $lat_decimal_degrees
        }
        $decimal_lat = sprintf("%2.5f",$decimal_lat);
        debug_print(3,"Decimal LAT: $decimal_lat\n");

        $long_degrees = substr($gpgll_long,0,3);
        $long_minutes = substr($gpgll_long,3,length($gpgll_long-3));
        debug_print(3,"LONG_DEGREES $long_degrees LONG_MINUTES $long_minutes\n");      
        $long_decimal_degrees = $long_minutes / 60;
        if ($gpgll_long_compass eq "W") {
            $decimal_long = 0 - ($long_degrees + $long_decimal_degrees)    
        } else {
            $decimal_long = $long_degrees + $long_decimal_degrees
        }
        $decimal_long = sprintf("%3.5f",$decimal_long);
        debug_print(3,"Decimal LONG: $decimal_long\n");
   }
   # parse cradlepoint R500
   # Data: -81,-5,30.0,100,315010,1,56140
   
   if ($line =~ /Data:\s([\-\d]+)\,([\-\d\.]+)\,([\-\d\.]+)\,(\d+)\,(\d+)\,(\d+)\,(\d+)/) {
       debug_print(2,"cradlepoint R500 line detected standard:\n$line");
       $rsrp = $1;
       $rsrq = $2;
       $sinr = $3;
       $signalstr = $4;
       $plmn = $5;
       $cellid = $6;
       $rfchannel = $7;

       $r500_file_flag = 1;
       $r500_seen_flag = 1;
   }
   # parse Baicells OD04 data
   #12:58:59 - dBm - dBm - - - - - - - - - -
   if ($line =~ /(\d\d:\d\d:\d\d)\s\-\sdBm\s\-/) {
        debug_print(2,"Baicells OD line detected dashes:\n$line");
        $time = $1;
        $rsrp1 = -999;
        $rsrp2 = -999;
        $rsrq = -999;
        $sinr1 = -999;
        $sinr2 = -999;
        $plmn      = "NaN"; #print "plmn $plmn\n";
        $earfcn    = "NaN"; #print "earfcn $earfcn\n";
        $cellid    = "NaN"; #print "cellid $cellid\n";
        $pci       = "NaN"; #print "pci $pci\n";
        $dlfreq    = "NaN"; #print "dlfreq $dlfreq\n";
        $ulfreq    = "NaN"; #print "ulfreq $ulfreq\n";
        $bandwidth = 0;

     $baicells_od_seen_flag = 1;
     $baicells_od_file_flag = 1;     
   }
   #             15:01:32         -83.8 dBm                 -80.8 dBm  -7.1          32.9        33.6          315010    55540        270082       1          3580000 kHz      3580000     kHz      20 M
   if ($line =~ /(\d\d:\d\d:\d\d)\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\s([\-\d\.]+)\s([\-\d\.]+)\s([\-\d]+)\s([\-\d]+)\s+([\-\d]+)\s+([\-\d]+)\s+([\-\d]+)\s+kHz\s+([\-\d]+)\s+\w*\s*([\-\d]+)/) {
        debug_print(2,"Baicells OD line detected standard:\n$line");
        $time      = $1; #print "time $time\n";
        $rsrp1     = $2; #print "rsrp1 $rsrp1\n";
        $rsrp2     = $3; #print "rsrp2 $rsrp2\n";
        $rsrq      = $4; #print "rsrq $rsrq\n";
        $sinr1     = $5; #print "sinr1 $sinr1\n";
        $sinr2     = $6; #print "sinr2 $sinr2\n";
        $plmn      = $7; #print "plmn $plmn\n";
        $earfcn    = $8; #print "earfcn $earfcn\n";
        $cellid    = $9; #print "cellid $cellid\n";
        $pci       = $10; #print "pci $pci\n";
        $dlfreq    = $11; #print "dlfreq $dlfreq\n";
        $ulfreq    = $12; #print "ulfreq $ulfreq\n";     
        $bandwidth = $13; #print "bandwidth $bandwidth\n";

        $baicells_od_seen_flag = 1;
        $baicells_od_file_flag = 1;
   }
   # parse Baicells ID06 data
   if ($line =~ /(\d\d:\d\d:\d\d)\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\sdBm\s([\-\d\.]+)\s\/\s([\-\d\.]+)\s\/\s([\-\d\.]+)\s\/\s([\-\d\.]+)\s([\-\d\.]+)\s+\/\s+([\-\d\.]+)\s+\/\s+([\-\d\.]+)\s+\/\s+([\-\d\.]+)\s+([\-\d\.]+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+([\.\d]+)\s+([\.\d]+)\s+(\d+)/) {
        debug_print(2,"Baicells ID line detected:\n$line");
        $time = $1; #print "time $time\n";
        $rsrp1 = $2; #print "rsrp1 $rsrp1\n";
        $rsrp2 = $3; #print "rsrp2 $rsrp2\n";
        $rsrp3 = $4; #print "rsrp3 $rsrp3\n";
        $rsrp4 = $5; #print "rsrp4 $rsrp4\n";
        $rssi1 = $6; #print "rssi1 $rssi1\n";
        $rssi2 = $7; #print "rssi2 $rssi2\n";
        $rssi3 = $8; #print "rssi3 $rssi3\n";
        $rssi4 = $9; #print "rssi4 $rssi4\n";
        $rsrq1 = $10; #print "rsrq1 $rsrq1\n";
        $rsrq2 = $11; #print "rsrq2 $rsrq2\n";
        $rsrq3 = $12; #print "rsrq3 $rsrq3\n";
        $rsrq4 = $13; #print "rsrq4 $rsrq4\n";
        $sinr = $14; #print "sinr $sinr\n";
        $plmn = $15; #print "plmn $plmn\n";
        $earfcn = $16; #print "earfcn $earfcn\n";
        $cellid = $17; #print "cellid $cellid\n";
        $pci = $18; #print "pci $pci\n";
        $dlfreq = $19; #print "dlfreq $dlfreq\n";
        $ulfreq = $20; #print "ulfreq $ulfreq\n";     
        $bandwidth = $21; #print "bandwidth $bandwidth\n";

        $baicells_id_seen_flag = 1;
        $baicells_id_file_flag = 1;
    }
    # parse iperf3 data
    # iperf3 data over multiple lines, need to track UL/DL mode, and throughput
    if ($line =~ /Connecting\s+to\s+host/) {
        debug_print(2,"iperf3 CONNECTING line detected:\n$line");  
        $reverse_seen = 0; 
        $iperf_mode = ""; 
        $throughput = "0.0";
    }
    if ($line =~ /Reverse/) {
        debug_print(2,"iperf3 REVERSE line detected:\n$line");  
        $reverse_seen = 1;
    }
    if ($line =~ /local/) {
        debug_print(2,"iperf3 LOCAL line detected:\n$line");
        if ($reverse_seen == 1) {
        $iperf_mode = $DOWNLOAD;
        } else {
        $iperf_mode = $UPLOAD;
        }
        debug_print(2,"iperf3 mode detected iperf_mode: $iperf_mode\n");
    }
    # bits/sec, Mbits/sec, Kbits/sec
    if ($line =~ /([\d\.]+)\s+(\w+)\/sec\s+[\.\w]+\s+ms\s+\d+\/\d+\s+\(\d+\%\)\s+receiver/ ) {
        $throughput = sprintf("%3.3f",$1);
        my $iperf_units = $2;
        debug_print(2,"iperf3 throughput line detected: $line\n");
        debug_print(3,"iperf3 tp: $throughput units: $iperf_units\n");
        # normalize to Mbits/sec
        if ($iperf_units eq "Kbits") {
            $throughput = sprintf("%3.3f",$throughput/1000.0);
            debug_print(3,"iperf3 Kbits converted to Mbits, tp: $throughput\n");
        }
        if ($iperf_units eq "bits") {
            $throughput = sprintf("%3.3f",$thoughput/1000000.0);
            debug_print(3,"iperf3 bits converted to Mbits, tp: $throughput\n");
        }
        $iperf_throughput_flag = 1;
    }
    # parse Android termux location and rsrp
    if ($line =~ /\"latitude\"\:\s+([\d\-\.]+)/) {
        $termux_latitude = $1;
        debug_print(2,"FOUND LATITUDE\n$line");
    }
    if ($line =~ /\"longitude\"\:\s+([\d\-\.]+)/) {
        $termux_longitude = $1;
        debug_print(2,"FOUND LONGITUDE\n$line");
    }
    if ($line =~ /\"registered\"\:\strue\,/) {
        debug_print(2,"FOUND REGISTERED\n$line");
        $termux_registered = 1;
    }
    if ($line =~ /\"registered\"\:\sfalse\,/) {
        debug_print(2,"FOUND NOT REGISTERED\n$line");
        $termux_registered = 0;
    }
    if ($line =~ /\"dbm\"\:\s([-,\d]+)\,/) {
        $termux_rsrp = $1;
        $termux_dbm_seen_flag = 1;
        $termux_file_flag = 1;      
    }

    # process lines seen
    if ($r500_seen_flag == 1) {
        if ($rsrp == 0) {
            $rsrp = -999;
        }
        if ($drive_mode_flag == 1) {
            print $decimal_lat.",".$decimal_long.",".$rsrp.",".$rsrq.",".$sinr.",".$signalstr.",".$plmn.",".$cellid.",".$rfchannel."\n";
        } else {
            $num_rsrp_entries++;
            $rsrp_square_sum_all += $rsrp*$rsrp;
            $rsrq_square_sum += $rsrq*$rsrq;
            $num_rsrq_entries++;
            $num_sinr_entries++;
            $sinr_sum_all += $sinr;   
            $num_signalstr_entries++;
            $signalstr_sum += $signalstr;
        }
    }
    if (($baicells_id_seen_flag == 1) or ($baicells_od_seen_flag)) {
        if ($rsrp1 == 0) {
            $rsrp1 = -999;
        }
        if ($rsrp2 == 0) {
            $rsrp2 = -999;
        }

        $num_rsrp_entries+=2; # remember when calculating best RMS, we need to divide this by 2 for RMS
        $rsrp_square_sum_all += $rsrp1*$rsrp1; 
        $rsrp_square_sum_all += $rsrp2*$rsrp2;
        if ($rsrp1 >= $rsrp2) {
            debug_print(3,"rsrp1 is best\n");
            $rsrp_square_sum_best += $rsrp1*$rsrp1;
        } else {
            debug_print(3,"rsrp2 is best\n");
            $rsrp_square_sum_best += $rsrp2*$rsrp2;
        }

        $rsrq_square_sum += $rsrq*$rsrq;
        $num_rsrq_entries++;

        $num_sinr_entries+=2; # remember when calculating best RMS, we need to divide this by 2 for RMS
        $sinr_sum_all += $sinr1; 
        $sinr_sum_all += $sinr2;
        if ($sinr1 >= $sinr2) {
            debug_print(3,"sinr1 is best\n");
            $sinr_sum_best += $sinr1;
        } else {
            debug_print(3,"sinr2 is best\n");
            $sinr_sum_best += $sinr2;
        }
    }
    if ($iperf_throughput_flag == 1) {
        if ($iperf_mode eq $DOWNLOAD) {
            $dl_square_sum += $throughput*$throughput;
            $dl_entries_count++;
        } elsif ($iperf_mode eq $UPLOAD) {
            $ul_square_sum += $throughput*$throughput;
            $ul_entries_count++;
        } else {
            die "ERROR: Unexpected iperf_mode\n";
        }
    }
    if ($termux_dbm_seen_flag == 1) {
        # only process if registered
        if ($termux_registered == 1) {
            debug_print(2, "FOUND DBM LINE\n");
            $termux_entries_count++;
            $termux_rsrp_square_sum += $termux_rsrp*$termux_rsrp; 
        }
    }
    
}
close(INPUTFILE);

# end of file processing
if (($r500_file_flag == 1) and ($drive_mode_flag != 1)) {
    debug_print(2, "EOF processing r500\n");  
    $rsrp_rms_all = sqrt($rsrp_square_sum_all/$num_rsrp_entries);
    $rsrp_rms_all = sprintf("%3.1f",$rsrp_rms_all);
    print $decimal_lat.",".$decimal_long.",-".$rsrp_rms_all."/";
    if ($dl_entries_count > 0) {
        $dl_rms = sqrt($dl_square_sum/$dl_entries_count);
        $dl_rms = sprintf("%3.1f",$dl_rms);
        print $dl_rms;
    } else {
        print "NaN";
    }
    print "/";
    if ($ul_entries_count > 0) {
        $ul_rms = sqrt($ul_square_sum/$ul_entries_count);
        $ul_rms = sprintf("%3.1f",$ul_rms);
        print $ul_rms;
    } else {
        print "NaN";
    }
    $rsrq_rms = sqrt($rsrq_square_sum/$num_rsrq_entries);
    $rsrq_rms = sprintf("%3.1f",$rsrq_rms);
    print ",-".$rsrq_rms;
    $sinr_mean_all = $sinr_sum_all/$num_sinr_entries;
    $sinr_mean_all = sprintf("%3.1f",$sinr_mean_all);
    print ",".$sinr_mean_all;
    $signalstr_mean = sprintf("%3.1f",($signalstr_sum/$num_signalstr_entries));   
    print ",".$signalstr_mean;
    print ",".$plmn.",".$cellid.",".$rfchannel;
}
if (($baicells_id_file_flag == 1) or ($baicells_od_file_flag == 1)) {
    debug_print(2, "EOF processing Baicells\n");
    $rsrp_rms_all = sqrt($rsrp_square_sum_all/$num_rsrp_entries);
    $rsrp_rms_all = sprintf("%3.1f",$rsrp_rms_all);
    $rsrp_rms_best = sqrt($rsrp_square_sum_best/($num_rsrp_entries/2));
    $rsrp_rms_best = sprintf("%3.1f",$rsrp_rms_best);
    
    print $decimal_lat.",".$decimal_long.",-".$rsrp_rms_best."/"."-".$rsrp_rms_all;
    print "/";
    if ($dl_entries_count > 0) {
        $dl_rms = sqrt($dl_square_sum/$dl_entries_count);
        $dl_rms = sprintf("%3.1f",$dl_rms);
        print $dl_rms;
    } else {
        print "NaN";
    }
    print "/";
    if ($ul_entries_count > 0) {
        $ul_rms = sqrt($ul_square_sum/$ul_entries_count);
        $ul_rms = sprintf("%3.1f",$ul_rms);
        print $ul_rms;
    } else {
        print "NaN";
    }
    if ($baicells_od_file_flag == 1) {
        #$time      = $1; #print "time $time\n";
        ##$rsrq      = $4; #print "rsrq $rsrq\n";
        $rsrq_rms = sqrt($rsrq_square_sum/$num_rsrq_entries);
        $rsrq_rms = sprintf("%3.1f",$rsrq_rms);
        print ",-".$rsrq_rms;
        ##$sinr1     = $5; #print "sinr1 $sinr1\n";
        ##$sinr2     = $6; #print "sinr2 $sinr2\n";
        $sinr_mean_all = $sinr_sum_all/$num_sinr_entries;
        $sinr_mean_all = sprintf("%3.1f",$sinr_mean_all);
        $sinr_mean_best = $sinr_sum_best/($num_sinr_entries/2);
        $sinr_mean_best = sprintf("%3.1f",$sinr_mean_best);
        print ",".$sinr_mean_best."/".$sinr_mean_all;
        #$plmn      = $7; #print "plmn $plmn\n";
        #$earfcn    = $8; #print "earfcn $earfcn\n";
        #$cellid    = $9; #print "cellid $cellid\n";
        #$pci       = $10; #print "pci $pci\n";
        #$dlfreq    = $11; #print "dlfreq $dlfreq\n";
        #$ulfreq    = $12; #print "ulfreq $ulfreq\n";     
        #$bandwidth = $13;
        print ",".$plmn.",".$earfcn.",".$cellid.",".$pci.",".$dlfreq.",".$ulfreq.",".$bandwidth;
    }
}
if (($rsrp1 == 0) and ($termux_file_flag != 1) and ($r500_file_flag != 1)) {
    debug_print(2, "EOF processing, bad RSRP\n");
    print $decimal_lat.",".$decimal_long.",,,,,,,,,,\n";
}

if ($termux_file_flag == 1) {
    debug_print(2, "EOF processing termux\n");
    if ($termux_rsrp_square_sum != 0) {
        $termux_rsrp_rms = sqrt($termux_rsrp_square_sum /$termux_entries_count);
        $termux_rsrp_rms = sprintf("%3.1f",$termux_rsrp_rms);
    } else {
        $termux_rsrp_rms = "NaN";
    }

    print $termux_latitude.",".$termux_longitude.",-".$termux_rsrp_rms;     
}
if (defined($options{n})) {
    print ",".$notes;
}
if ($drive_mode_flag != 1) {
    print "\n";
}