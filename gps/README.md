# README #

This README documents steps necessary to parse a GPS puck with macOS.

### What is this repository for? ###

* Quick summary

This script returns the first GPGLL line from a connected GPS puck.


* Version

July 7, 2021

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Connect the GPS puck to your laptop.

* Configuration

Nothing to configure.

* Dependencies

This script assumes using a GPS puck similar to https://www.amazon.com/gp/product/B073P3Y48Q/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

The GPS puck is expected to use stty defaults (38400 baud).

This script expects that you only have 1 /dev/cu.usbmodem* device.  If you have more than one, then specify the full path to the GPS puck (i.e. /dev/cu.usbmodemXXX).

* How to run tests

You can run this script by itself to see if it can pickup the GPS puck automatically.

You can also run it with the /dev/cu.usbmodemXXX handle to force it to use that one, which you would need if more than one device matches /dev/cu.usbmodem*

* Deployment instructions

Check out the repository on the machine that is intended to have GPS puck attached.

### Who do I talk to? ###

* Repo owner or admin

Mark Jimenez, mjimenez@celona.io

* Other community or team contact

Greg Yatman, greg@celona.io
The Frequency community at https://frequency.celona.io/