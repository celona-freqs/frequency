#!/usr/bin/env python3
import sys
import re
import glob

if(len(sys.argv) > 1):
    gpsfilepath = sys.argv[1]
else: 
    possibles = glob.glob("/dev/cu.usbmodem*")
    if(len(possibles) != 1):
        print("Number of cu.usbmodem devices is not 1. Exiting, please specify handle.")
        sys.exit()
    else:
        gpsfilepath = possibles[0]   
print("reading", format(gpsfilepath))
good = False
f = open (gpsfilepath, "r")
while(good != True): 
  dataline = f.readline()
  #print(dataline)
  matchObj = re.match('\$GPGLL', dataline, re.M|re.I)
  if matchObj:
    #print ("Found match")
    print(dataline)
    good = True 
f.close()